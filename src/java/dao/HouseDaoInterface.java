/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.List;
import model.House;

/**
 *
 * @author Vi Tuan Vu
 */
public interface HouseDaoInterface {
    List<House> getAllHouse() throws SQLException;


    public List<House> getAllHouses() throws SQLException ;

    public List<House> getAllHousesAdmin() throws SQLException;
    
    

    
}
