/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.List;
import model.House;
import model.User;

/**
 *
 * @author Vi Tuan Vu
 */
public interface UserDaoInterface {
    User getUserProfilebyID(int id) throws SQLException;
    
    void update(int id, String firstName, String lastName, String email, String phone, int role) throws SQLException;
    
    User login(String username, String password) throws SQLException;
}
