/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.HouseDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.House;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vi Tuan Vu
 */
public class HouseDAOImpl implements HouseDaoInterface{

    /**
     *
     * @return
     * @throws java.sql.SQLException
     * @throws Exception
     */
    @Override
    public List<House> getAllHouse() throws SQLException {
        List<House> listHouses = new ArrayList<>();
        String sql = "Select name, imageURL, address from Houses";
        try (Connection conn = new DBContext().getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                House house = new House(rs.getString(1), rs.getString(3), rs.getString(2));
                listHouses.add(house);
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listHouses;
    }

    public House getHouseDetail(int id) {
        try {
            Connection conn = new DBContext().getConnection();

            String sql = "SELECT * FROM Houses\n"
                    + "where houseID = ?";

            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1, id);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    House house = new House();
                    house.setHouseID(rs.getInt(1));
                    house.setStatus(rs.getString(2));
                    house.setAddress(rs.getString(3));
                    house.setDescription(rs.getString(4));
                    house.setFeedback(rs.getString(5));
                    house.setCategoryID(rs.getInt(6));
                    house.setUserID(rs.getInt(7));
                    house.setName(rs.getString(8));
                    house.setImageURL(rs.getString(9));
                    return house;
                }
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<House> getSearchHouse(String keyword) {
        List<House> houses = new ArrayList<>();

        try {
            Connection conn = new DBContext().getConnection();

            String sql = "SELECT * FROM Houses\n"
                    + "where 'name' like '%?%'";

            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, keyword);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    House house = new House();
                    house.setHouseID(rs.getInt(1));
                    house.setStatus(rs.getString(2));
                    house.setAddress(rs.getString(3));
                    house.setDescription(rs.getString(4));
                    house.setFeedback(rs.getString(5));
                    house.setCategoryID(rs.getInt(6));
                    house.setUserID(rs.getInt(7));
                    house.setName(rs.getString(8));
                    house.setImageURL(rs.getString(9));
                    houses.add(house);
                }
                ps.close();
                return houses;
            } catch (SQLException ex) {
                Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return null;
        } catch (Exception ex) {
            Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    @Override
    public List<House> getAllHousesAdmin() throws SQLException {
        List<House> listHouses = new ArrayList<>();
        String sql = "SELECT [houseID]\n"
                + "      ,[status]\n"
                + "      ,[address]\n"
                + "      ,[description]\n"
                + "      ,[feedback]\n"
                + "      ,[categoryID]\n"
                + "      ,[userID]\n"
                + "      ,[imageURL]\n"
                + "      ,[name]\n"
                + "  FROM [Houses]";
        try (Connection conn = new DBContext().getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                House house = new House(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getString(8), rs.getString(9));
                listHouses.add(house);
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(HouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listHouses;
    }
    public static void main(String[] args) throws SQLException {
        HouseDAOImpl aO = new HouseDAOImpl();
        System.out.println(aO.getAllHousesAdmin().get(0).getName());
    }

    @Override
    public List<House> getAllHouses() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
