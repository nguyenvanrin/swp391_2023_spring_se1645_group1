/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import model.User;
import context.DBContext;
import dao.UserDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class UserDAOImpl implements UserDaoInterface{

    public User getUserProfilebyID(int id){
        try {
            String sql = "SELECT [userID]\n"
                    + "      ,[roleID]\n"
                    + "      ,[firstName]\n"
                    + "      ,[lastName]\n"
                    + "      ,[Email]\n"
                    + "      ,[Phone]\n"
                    + "      ,[avatarURL]\n"
                    + "      ,[Password]\n"
                    + "      ,[status]\n"
                    + "  FROM [Users]\n"
                    + "  WHERE userID = ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9));

                return user;
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

  

    public void update(int id, String firstName, String lastName, String email, String phone, int role) {
        try {
            String sql = "UPDATE [Users]\n"
                    + "   SET [roleID] = ?\n"
                    + "      ,[firstName] = ?\n"
                    + "      ,[lastName] = ?\n"
                    + "      ,[Email] = ?\n"
                    + "      ,[Phone] = ?\n"
                    + " WHERE userID =?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setString(2, firstName);
            ps.setString(3, lastName);
            ps.setString(4, email);
            ps.setString(5, phone);
            ps.setInt(6, id);

            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public User login(String username, String password) {
        try {
            String sql = "SELECT [userID]\n"
                    + "      ,[roleID]\n"
                    + "      ,[firstName]\n"
                    + "      ,[lastName]\n"
                    + "      ,[Email]\n"
                    + "      ,[Phone]\n"
                    + "      ,[avatarURL]\n"
                    + "      ,[Password]\n"
                    + "      ,[status]\n"
                    + "  FROM [Users]\n"
                    + "  WHERE [Email] = ? AND [Password]= ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User account = new User(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9));
                return account;
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
