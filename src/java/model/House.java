/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Vi Tuan Vu
 */
public class House {
    private int houseID;
    private int categoryID;
    private int userID;
    private String status;
    private String address;
    private String description;
    private String feedback;
    private String imageURL;
    private String name;
    public House() {
    }
    public House(String name,String address,String imageURL){
        this.name = name;
        this.address = address;
        this.imageURL = imageURL;
    }

    public House(int houseID, String status,  String address, String description, String feedback, int categoryID, int userID,  String imageURL, String name) {
        this.houseID = houseID;
        this.categoryID = categoryID;
        this.userID = userID;
        this.status = status;
        this.address = address;
        this.description = description;
        this.feedback = feedback;
        this.imageURL = imageURL;
        this.name = name;
    }

    public int getHouseID() {
        return houseID;
    }

    public void setHouseID(int houseID) {
        this.houseID = houseID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    

    
    
}
