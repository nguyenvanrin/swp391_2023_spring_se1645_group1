<%-- 
    Document   : resetPassword
    Created on : Feb 20, 2023, 2:28:20 PM
    Author     : Vi Tuan Vu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reset password</title>
        <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
    </head>
    <body>
        <div align="center">
            <h2>Reset Your Password</h2>
            <p>
                Please enter your login email, we'll send a new random password to your inbox:
            </p>

            <form id="resetForm" action="reset_password" method="post">
                <table>
                    <tr>
                        <td>Email:</td>
                        <td><input type="text" name="email" id="email" size="20"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button type="submit">Send me new password</button>
                        </td>
                    </tr>    
                </table>
            </form>
        </div>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#resetForm").validate({
                    rules: {
                        email: {
                            required: true,
                            email: true
                        }
                    },

                    messages: {
                        email: {
                            required: "Please enter email",
                            email: "Please enter a valid email address"
                        }
                    }
                });

            });
        </script>
    </body>
</html>
