<%-- 
    Document   : houseDetail
    Created on : Feb 16, 2023, 11:56:53 PM
    Author     : Hoang Tran
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${requestScope.house == null}">
                <h2>Can not find house</h2>
        </c:if>
        <c:if test="${requestScope.house != null}">
            <p>${house.houseID}</p>
            <p>${house.categoryID}</p>
            <p>${house.userID}</p>
            <p>${house.status}</p>
            <p>${house.address}</p>
            <p>${house.description}</p>
            <p>${house.feedback}</p>
            <p>${house.feedback}</p>
        </c:if>
                
    </body>
</html>
