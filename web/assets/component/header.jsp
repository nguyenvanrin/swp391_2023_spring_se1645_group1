<%-- 
    Document   : header
    Created on : Feb 14, 2023, 10:05:50 AM
    Author     : Vi Tuan Vu
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header>
    <div id="wrapper" class="bg-light">
        <div id="header">
            <a href="" class="logo" style="text-decoration: none">
                <img src="assets/image/logoFPT.png" alt="" />
                <h6>FU House Finder</h6>
            </a>
            <nav>
                <div id="menu">
                    <div class="item">
                        <a href="#">Homes</a>
                    </div>
                    <div class="item">
                        <a href="#">About</a>
                    </div>
                    <div class="item">
                        <a href="#">House</a>
                    </div>
                    <div class="item">
                        <a href="#">Room</a>
                    </div>
                    <c:choose>
                        <c:when test="${sessionScope.roleID == 1}">
                            <div class="item">
                                <a href="manager-admin">Manager Admin</a>
                            </div>
                        </c:when>
                         <c:when test="${sessionScope.roleID == 2}">
                            <div class="item">
                                <a href="#">Manager Lanlord</a>
                            </div>
                            <div class="item">
                                <a href="#">Report</a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="item">
                                <a href="#">Report</a>
                            </div>
                        </c:otherwise>
                    </c:choose>

                </div>
            </nav>
            <div id="actions">
                <div class="item">
                    <c:choose>
                        <c:when test="${sessionScope.account != null}">

                            <a href="view-profile-user?id=${sessionScope.account.userID}" style="text-decoration: none">
                                <button class="btn btn-outline-primary ms-lg-2">${sessionScope.account.firstName} ${sessionScope.account.lastName}</button>
                            </a>
                            <a href="logout" class="btn btn-outline-primary ms-lg-2">Logout</a>


                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-primary" href="login">Login</a>
                        </c:otherwise>
                    </c:choose>

                </div>
                <div class="item">
                    <img src="assets/image/cart.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</header>
