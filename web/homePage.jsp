
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>FU House Finder</title>
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="assets/css/responsize.css" />
        <link rel="stylesheet" href="assets/css/slide.css" />
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script
            type="text/javascript"
            src="https://itexpress.vn/API/files/it.snow.js"
        ></script>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
            />
    </head>

    <body style="background-color: bisque">
        <%@include file="assets/component/header.jsp" %>
        <div class="container">
            <div class="row height d-flex justify-content-center align-items-center">
                <div class="col-md-8">
                    <div class="search text-center">
                        <form>
                            <input
                                type="text"
                                class="form-control"
                                placeholder="Find room,house"
                                />
                            <button class="btn btn-primary btn-search">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="slideshow-container">
            <div class="mySlides fade">
                <div class="numbertext">1 / 3</div>
                <img
                    class="imgSlide"
                    src="assets/image/Slide/Nha tro.png"
                    style="width: 100%"
                    />
            </div>

            <div class="mySlides fade">
                <div class="numbertext">2 / 3</div>
                <img
                    class="imgSlide"
                    src="assets/image/Slide/phong tro.jpg"
                    style="width: 100%"
                    />
            </div>

            <div class="mySlides fade">
                <div class="numbertext">3 / 3</div>
                <img
                    class="imgSlide"
                    src="assets/image/Slide/nha tro 2.png"
                    style="width: 100%"
                    />
            </div>
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br />
        <div style="text-align: center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
        </div>
        <div class="text-center">
            <h4>
                Hãy đến với chúng tôi để trải nghiệm những dịch vụ tốt nhất tại Hòa Lạc
                này
            </h4>
            <h4>FU House Finder - niềm tin của mọi sinh viên</h4>
            <br />
        </div>
        <div class="container">
            <div class="hotHouse">
               

                <div class="row imgHouse">
                    <c:forEach items="${listHouses}" var="p">
                        <div class="col text-center">
                            <img
                                src="${p.imageURL}"
                                />
                            <h4>${p.name}</h4>
                            <h4>${p.address}</h4>
                        </div>
                    </c:forEach>
                </div>


            </div>
            <br />
            <div class="bigHouse">
                
                <div class="row imgHouse">
                    <div class="col text-center">
                        <img src="assets/image/Gà Ri Apartment/gà ri.jpg" />
                        <h4>Gà Ri Apartment</h4>
                        <h4>thôn 9, Thạch Hòa, Thạch Thất</h4>
                    </div>
                    <div class="col text-center">
                        <img src="assets/image/Gà Ri Apartment/gà ri.jpg" />
                        <h4>Gà Ri Apartment</h4>
                        <h4>thôn 9, Thạch Hòa, Thạch Thất</h4>
                    </div>
                    <div class="col text-center">
                        <img src="assets/image/Gà Ri Apartment/gà ri.jpg" />
                        <h4>Gà Ri Apartment</h4>
                        <h4>thôn 9, Thạch Hòa, Thạch Thất</h4>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <footer class="bg-light text-center text-lg-start">
        <%@include file="assets/component/footer.jsp" %>
    </footer>
    <script src="assets/js/script.js"></script>
    <script src="assets/js/scriptSlide.js"></script>
    <script>
                AOS.init();
    </script>
</html>