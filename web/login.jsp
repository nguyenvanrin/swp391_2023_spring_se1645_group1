<%-- 
    Document   : login
    Created on : Feb 17, 2023, 12:59:15 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">      
        <title>Login</title>
    </head>
    <body>
        
         <div style="width: 40%; margin:0 auto; border:1px solid #ccc; padding: 1rem; background-color: #FFD495" class="mt-5">
            <h3>Login</h3>
            <form action="login" method="POST">
                <div class="mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember">
                    <label class="form-check-label" for="exampleCheck1">Remember me</label>
                </div>
                <h4 class="text-danger" style="">${error}</h4>
                <button type="submit" class="btn btn-primary">Login</button>
                <a class="btn btn-primary" href="home" style="">BACK</a>
                </br>
                <a class="mt-5" href="create-account">Create new account?</a>
                <br>
                <a class="mt-5" href="reset_password">Forgot password</a>
            </form>
               
        </div>
        
    </body>
</html>
