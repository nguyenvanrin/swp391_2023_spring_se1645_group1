<%-- 
    Document   : ViewprofileUser
    Created on : Feb 14, 2023, 2:54:49 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="view-profile-user" method="POST">
            <section style="background-color: #eee;">
                <div class="container py-5 ">
                    <div class="col-lg-6" style="margin: 0 auto;">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="row">
                                    <input type="hidden" value="${userProfile.userID}" name="id">
                                    <div class="col-sm-3">
                                        <p class="mb-0">First Name</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="text-muted mb-0" name="firstName" style="border: 0px" value="${userProfile.firstName}"/>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Last Name</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="text-muted mb-0" name="lastName" style="border: 0px" value="${userProfile.lastName}"/>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Email</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" readonly="" class="text-muted mb-0" name="email" style="border: 0px" value="${userProfile.email}"/>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Phone</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="text-muted mb-0" name="phone" style="border: 0px" value="${userProfile.phone}"/>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="btn-group">
                                        <input type="radio" class="btn-check" name="options" id="option1" value="2"  ${userProfile.roleID == 2?"Checked":""} />
                                        <label class="btn btn-secondary" for="option1">Guest</label>

                                        <input type="radio" class="btn-check" name="options" id="option2" value="3" ${userProfile.roleID == 3?"Checked":""}/>
                                        <label class="btn btn-secondary" for="option2">Landlord</label>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body text-center">
                                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp" alt="avatar"
                                     class="rounded-circle img-fluid" style="width: 150px;">
                                <button class="btn-dark" style="margin-left: 20px;">Browse</button>
                            </div>
                            <div class="text-center" style="margin-top: 30px; margin-bottom: 20px;">
                                <button type="submit" class="btn-dark" style="max-width: 100px;">Update</button>
                                <a class="btn btn-dark" href="home">BACK</a>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
            </section>
        </form>
    </body>
</html>
